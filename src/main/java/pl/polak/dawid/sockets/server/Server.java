package pl.polak.dawid.sockets.server;

import pl.polak.dawid.sockets.server.service.ServerControl;
import pl.polak.dawid.sockets.server.service.support.ServerTime;
import pl.polak.dawid.sockets.server.service.support.TimeStarter;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDate;

public class Server {

    private static final String SERVER_VERSION = "0.3";
    private static final int PORT = 3333;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private ServerTime serverTime;

    public void startServer() {
        try {
            serverSocket = new ServerSocket(PORT);
            System.out.println("Server is running...");
            serverTime = new ServerTime(new TimeStarter());

            clientSocket = serverSocket.accept();
            System.out.println("Server has started and listening to port: " + PORT);
            ServerControl serverControl = new ServerControl(this);
            serverControl.run();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public String getServerVersion() { return SERVER_VERSION;}

    public ServerTime getServerTime() {
        return serverTime;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public LocalDate getStartDateOfServer() { return serverTime.getStartDateOfServer();}
}