package pl.polak.dawid.sockets.server.repository;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnector {

    private static final String URL = "jdbc:postgresql://localhost:5432/db_sockets";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "1234";
    private DSLContext context;

    public DbConnector() {
        try {
            this.context = DSL
                    .using(DriverManager.getConnection(URL, USERNAME, PASSWORD), SQLDialect.POSTGRES);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public DSLContext getContext() {
        return context;
    }
}
