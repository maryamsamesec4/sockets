package pl.polak.dawid.sockets.server.service.support;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;

import static java.util.concurrent.TimeUnit.SECONDS;

public class ServerTime {

    private static final int TWENTY_FOUR_HOURS = 24;
    private static final int SECONDS_IN_MINUTES = 60;
    private final TimeStarter timeStarter;
    private final Instant startTimeOfServer;
    private final LocalDate startDateOfServer;

    public ServerTime(TimeStarter timeStarter) {
        this.timeStarter = timeStarter;
        startTimeOfServer = timeStarter.getStartTimeOfServer();
        startDateOfServer = LocalDate.now();
    }

    private long getDurationInSeconds() {
        return Duration.between(startTimeOfServer, timeStarter.getStartTimeOfServer()).getSeconds();
    }

    public Uptime getUptime() {
        long duration = getDurationInSeconds();
        int days = (int) SECONDS.toDays(duration);
        int hours = (int) (SECONDS.toHours(duration) - (days * TWENTY_FOUR_HOURS));
        int minutes = (int) (SECONDS.toMinutes(duration) - (SECONDS.toHours(duration) * SECONDS_IN_MINUTES));
        int seconds = (int) (SECONDS.toSeconds(duration) - (SECONDS.toMinutes(duration) * SECONDS_IN_MINUTES));

        return new Uptime(days, hours, minutes, seconds);
    }

    public LocalDate getStartDateOfServer() {
        return startDateOfServer;
    }
}
