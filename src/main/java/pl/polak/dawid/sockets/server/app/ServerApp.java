package pl.polak.dawid.sockets.server.app;

import pl.polak.dawid.sockets.server.Server;

public class ServerApp {

    public static void main(String[] args) {

        Server server = new Server();
        server.startServer();
    }
}
