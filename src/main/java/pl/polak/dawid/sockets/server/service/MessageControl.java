package pl.polak.dawid.sockets.server.service;

import pl.polak.dawid.sockets.server.domain.Message;
import pl.polak.dawid.sockets.server.domain.Role;
import pl.polak.dawid.sockets.server.domain.User;
import pl.polak.dawid.sockets.server.repository.MessageManagerBuilder;
import pl.polak.dawid.sockets.server.repository.UserManagerBuilder;

import java.util.LinkedList;

public class MessageControl {

    private static final int MAX_CAPACITY = 5;
    private final MessageManagerBuilder messageBuilder;
    private final UserManagerBuilder userBuilder;

    public MessageControl(MessageManagerBuilder messageBuilder,
                          UserManagerBuilder userBuilder) {
        this.messageBuilder = messageBuilder;
        this.userBuilder = userBuilder;
    }

    public void sendMessage(String username, String body) {
        User owner = userBuilder.loadUserByName(username);

        Integer senderId =  UserControl.currentUser.getUserId();
        Message message = new Message(owner.getUserId(), body, senderId);
        addMessage(message, owner);
    }

    protected void addMessage(Message message, User user) {
        LinkedList<Message> messages = messageBuilder.loadUserMessagesByUserId(user.getUserId());
        if (user.getRole() == Role.ADMIN) {
            messageBuilder.saveMessage(message);
        } else if (messages.size() < MAX_CAPACITY) {
            messageBuilder.saveMessage(message);
        } else {
            throw new IllegalStateException("Sending message failed, inbox is full (Inbox" +
                    " offer " + MAX_CAPACITY + " message storage).");
        }
    }

    public LinkedList<Message> readUserMessages() {
        LinkedList<Message> messages = messageBuilder.loadUserMessagesByUserId(UserControl.currentUser.getUserId());
        messages.forEach(System.out::println);

        if (messages.isEmpty()) {
            throw new IllegalStateException("Your inbox is empty, no messages to read");
        }
        return messages;
    }

    public LinkedList<Message> readMessagesFromOtherUser(String username) {
        User user = userBuilder.loadUserByName(username);
        LinkedList<Message> userMessages = messageBuilder.loadUserMessagesByUserId(user.getUserId());
        userMessages.forEach(System.out::println);

        if (userMessages.isEmpty()) {
            throw new IllegalStateException("User inbox is empty, no messages to read");
        }
        return userMessages;
    }

    public void deleteMessagesFromInbox() {
        Integer ownerId = UserControl.currentUser.getUserId();
        messageBuilder.clearInbox(ownerId);
    }

    public void deleteMessages(String username) {
        User user = userBuilder.loadUserByName(username);
        messageBuilder.clearInbox(user.getUserId());
    }
}
