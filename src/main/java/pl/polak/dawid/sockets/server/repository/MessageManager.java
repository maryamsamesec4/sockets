package pl.polak.dawid.sockets.server.repository;

import pl.polak.dawid.sockets.server.domain.Message;

import java.util.LinkedList;

public interface MessageManager {

    void saveMessage(Message message);

    LinkedList<Message> loadUserMessagesByUserId(int ownerId);

    void clearInbox(int ownerId);
}
